<?php
defined('_AKEEBA_RESTORATION') or die('Restricted access');
$restoration_setup = array(
	'kickstart.security.password' => '1uzL6UqActt7eUMedGDMRaCq75UDpzBd',
	'kickstart.tuning.max_exec_time' => '5',
	'kickstart.tuning.run_time_bias' => '75',
	'kickstart.tuning.min_exec_time' => '0',
	'kickstart.procengine' => 'direct',
	'kickstart.setup.sourcefile' => '/var/www/teamnumb.com/www/tmp/Joomla_3.7.0-Stable-Update_Package.zip',
	'kickstart.setup.destdir' => '/var/www/teamnumb.com/www',
	'kickstart.setup.restoreperms' => '0',
	'kickstart.setup.filetype' => 'zip',
	'kickstart.setup.dryrun' => '0');