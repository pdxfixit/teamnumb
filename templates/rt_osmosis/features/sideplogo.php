<?php
/**
 * @version   $Id: sideplogo.php 20884 2014-05-06 16:52:45Z arifin $
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 * Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */

defined('JPATH_BASE') or die();

gantry_import('core.gantryfeature');
/**
 * @package     gantry
 * @subpackage  features
 */
class GantryFeatureSideplogo extends GantryFeature
{
	var $_feature_name = 'sideplogo';

	function render($position)
	{
		global $gantry;
		ob_start();
		?>
        <div class="sideplogo-block">
            <a href="<?php echo $gantry->baseUrl; ?>" id="rt-sidelogo"></a>
        </div>
	<?php
		return ob_get_clean();
	}
}
