<?php
/**
 * @version   $Id: sidepmenu.php 21376 2014-06-01 17:38:10Z arifin $
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 * Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */

defined('JPATH_BASE') or die();

gantry_import('core.gantryfeature');
/**
 * @package     gantry
 * @subpackage  features
 */
class GantryFeatureSidepmenu extends GantryFeature
{
	var $_feature_name = 'sidepmenu';

	function render($position) {
		global $gantry;

        $renderer = $gantry->document->loadRenderer('module');
        $options = array('style' => "sidepanelmenu");
        $module = JModuleHelper::getModule('mod_roknavmenu','_z_empty');

        $menu_type = "menu-".$gantry->get('menu-type', 'dropdownmenu');

        $params = $gantry->getParams($menu_type, true);

        $reg = new JRegistry();
        foreach ($params as $param_name => $param_value)
        {
            $reg->set($param_name, $param_value['value']);
        }
		$reg->set('style', 'sidepanelmenu');
        $module->params = $reg->toString();
        $rendered_menu = $renderer->render($module, $options);

        return $rendered_menu;
	}
}
